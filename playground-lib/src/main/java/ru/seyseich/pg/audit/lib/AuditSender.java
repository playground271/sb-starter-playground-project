package ru.seyseich.pg.audit.lib;

import javax.annotation.Nonnull;

public interface AuditSender {

    void send(@Nonnull AuditMessage message);
}
