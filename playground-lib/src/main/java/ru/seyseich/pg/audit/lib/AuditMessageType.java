package ru.seyseich.pg.audit.lib;

public enum AuditMessageType {

    CREATE,
    UPDATE,
    DELETE
}
