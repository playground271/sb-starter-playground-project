package ru.seyseich.pg.audit.lib;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AuditMessage {

    AuditMessageType type;

    long userId;

    String action;
}
