package ru.seyseich.pg.audit.lib;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;

@Slf4j
@RequiredArgsConstructor
public class LoggingAuditSenderImpl implements AuditSender {

    private final String url;

    @Override
    public void send(@Nonnull AuditMessage message) {
        log.info("Successfully \"sent\" audit message: {}. [{}]", message, url);
    }
}
