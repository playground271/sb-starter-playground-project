package ru.seyseich.pg.audit.lib.requestcontext;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

import static java.util.Objects.isNull;

@Component
@RequiredArgsConstructor
@ConditionalOnMissingBean(name = "RequestContextInitializingServletFilter")
public class RequestContextInitializingServletFilter extends OncePerRequestFilter {

    private final RequestContext requestContext;

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain
    ) throws ServletException, IOException {
        requestContext.setUserId(parseUserId(request));
        requestContext.setRequestId(getRequestId(request));

        filterChain.doFilter(request, response);
    }

    private long parseUserId(HttpServletRequest request) {
        try {
            return Long.parseLong(request.getHeader("user-id"));
        } catch (Exception exc) {
            throw new RequestContextException(
                    String.format("Failed to parse user id! Reason: %s", exc.getMessage()),
                    exc
            );
        }
    }

    private String getRequestId(HttpServletRequest request) {
        try {
            var value = request.getHeader("request-id");

            return isNull(value) ? UUID.randomUUID().toString() : value;
        } catch (Exception exc) {
            throw new RequestContextException(
                    String.format("Failed to parse request id! Reason: %s", exc.getMessage()),
                    exc
            );
        }
    }
}
