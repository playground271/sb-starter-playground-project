package ru.seyseich.pg.audit.lib.requestcontext;

import lombok.Setter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.isNull;

@RequestScope
@Component
public class RequestContext {

    @Setter
    private String requestId;

    @Setter
    private Long userId;

    @Nonnull
    public String getRequestId() {
        return validateAndGet(requestId, "requestId");
    }

    public long getUserId() {
        return validateAndGet(userId, "userId");
    }

    @Nonnull
    private <T> T validateAndGet(@Nullable T value, @Nonnull String field) {
        if (isNull(value)) {
            throw new RequestContextException(
                    String.format("Failed to get required data from request context! Field [%s] is null.", field)
            );
        }

        return value;
    }
}
