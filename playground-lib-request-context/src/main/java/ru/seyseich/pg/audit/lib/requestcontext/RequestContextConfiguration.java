package ru.seyseich.pg.audit.lib.requestcontext;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.seyseich.pg.audit.lib.requestcontext")
public class RequestContextConfiguration {
}
