package ru.seyseich.pg.audit.starter.autoconfigure;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.seyseich.pg.audit.lib.AuditSender;
import ru.seyseich.pg.audit.lib.LoggingAuditSenderImpl;
import ru.seyseich.pg.audit.lib.aspect.PlaygroundAuditAspectConfiguration;

import java.util.Objects;

@Configuration
@ConditionalOnClass(AuditSender.class)
@Import(PlaygroundAuditAspectConfiguration.class)
@EnableConfigurationProperties(PlaygroundAuditProperties.class)
public class PlaygroundAuditAutoConfiguration {

    @ConditionalOnMissingBean(AuditSender.class)
    @Bean
    public AuditSender auditSender(PlaygroundAuditProperties properties) {
        return new LoggingAuditSenderImpl(
                Objects.requireNonNull(properties.getServiceUrl(), "Audit service URL is not specified!")
        );
    }
}
