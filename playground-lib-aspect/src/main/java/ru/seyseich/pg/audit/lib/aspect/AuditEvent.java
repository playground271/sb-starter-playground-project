package ru.seyseich.pg.audit.lib.aspect;

import ru.seyseich.pg.audit.lib.AuditMessageType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(AuditEvents.class)
public @interface AuditEvent {

    AuditMessageType type();

    String action();
}
