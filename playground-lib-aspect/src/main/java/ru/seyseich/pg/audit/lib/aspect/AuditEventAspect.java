package ru.seyseich.pg.audit.lib.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.seyseich.pg.audit.lib.AuditMessage;
import ru.seyseich.pg.audit.lib.AuditSender;
import ru.seyseich.pg.audit.lib.requestcontext.RequestContext;

import javax.annotation.Nonnull;
import java.util.Arrays;

@Aspect
@Component
public class AuditEventAspect {

    private final AuditSender auditSender;

    private final RequestContext requestContext;

    public AuditEventAspect(@Autowired AuditSender auditSender, @Autowired RequestContext requestContext) {
        this.auditSender = auditSender;
        this.requestContext = requestContext;
    }

    @Around("@annotation(ru.seyseich.pg.audit.lib.aspect.AuditEvent)")
    @Nonnull
    public Object processAuditEvent(@Nonnull ProceedingJoinPoint joinPoint) throws Throwable {
        var result = joinPoint.proceed();
        var signature = (MethodSignature) joinPoint.getSignature();

        process(signature.getMethod().getAnnotation(AuditEvent.class));

        return result;
    }

    @Around("@annotation(ru.seyseich.pg.audit.lib.aspect.AuditEvents)")
    @Nonnull
    public Object processAuditEvents(@Nonnull ProceedingJoinPoint joinPoint) throws Throwable {
        var result = joinPoint.proceed();

        var signature = (MethodSignature) joinPoint.getSignature();
        var auditEventsAnnotation = signature.getMethod().getAnnotation(AuditEvents.class);

        Arrays.stream(auditEventsAnnotation.value()).forEach(this::process);

        return result;
    }

    private void process(AuditEvent auditEventAnnotation) {
        auditSender.send(
                AuditMessage.builder()
                        .action(auditEventAnnotation.action())
                        .type(auditEventAnnotation.type())
                        .userId(requestContext.getUserId())
                        .build()
        );
    }
}
