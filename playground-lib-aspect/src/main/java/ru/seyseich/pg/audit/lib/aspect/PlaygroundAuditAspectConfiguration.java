package ru.seyseich.pg.audit.lib.aspect;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.seyseich.pg.audit.lib.aspect")
public class PlaygroundAuditAspectConfiguration {
}
